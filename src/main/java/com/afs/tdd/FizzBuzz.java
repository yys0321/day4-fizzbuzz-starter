package com.afs.tdd;

public class FizzBuzz {
    public String say(int number) {
        if (number %3 == 0 && number %5 == 0 && number %7 == 0) {
            return "fizzbuzzwhizz";
        } else if (number %3 == 0 && number %5 == 0) {
            return "fizzbuzz";
        } else if (number %5 == 0 && number %7 == 0) {
            return "buzzwhizz";
        } else if (number %3 == 0 && number %7 == 0) {
            return "fizzwhizz";
        } else if (number %3 == 0) {
            return "fizz";
        } else if (number %5 == 0) {
            return "buzz";
        } else if (number %7 == 0) {
            return "whizz";
        } else {
            return String.valueOf(number);
        }
    }
}
