package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {

    @Test
    public void should_say_number_when_sayFizzBuzz_given_input_is_normal_number(){
//        given
        int number = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("1", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_is_3(){
//        given
        int number = 3;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("fizz", actual);
    }

    @Test
    public void should_say_buzz_when_sayFizzBuzz_given_input_is_5(){
//        given
        int number = 5;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("buzz", actual);
    }
    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_is_multiple_of_3(){
//        given
        int number = 6;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("fizz", actual);
    }

    @Test
    public void should_say_buzz_when_sayFizzBuzz_given_input_is_multiple_of_5(){
//        given
        int number = 10;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("buzz", actual);
    }

    @Test
    public void should_say_fizzbuzz_when_sayFizzBuzz_given_input_is_multiple_of_3_and_5(){
//        given
        int number = 30;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("fizzbuzz", actual);
    }

    @Test
    public void should_say_whizz_when_sayFizzBuzz_given_input_is_multiple_of_7(){
//        given
        int number = 14;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("whizz", actual);
    }

    @Test
    public void should_say_fizzbuzzwhizz_when_sayFizzBuzz_given_input_is_multiple_of_3_and_5_and_7(){
//        given
        int number = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("fizzbuzzwhizz", actual);
    }

    @Test
    public void should_say_buzzwhizz_when_sayFizzBuzz_given_input_is_multiple_of_5_and_7(){
//        given
        int number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("buzzwhizz", actual);
    }

    @Test
    public void should_say_fizzwhizz_when_sayFizzBuzz_given_input_is_multiple_of_3_and_7(){
//        given
        int number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();
//        when
        String actual = fizzBuzz.say(number);
//        then
        assertEquals("fizzwhizz", actual);
    }
}